variable "ALPINE_RELEASE" {
  default = "3.19"
}

variable "ALP_VER" {
  default = ""
}

variable "APK" {
  default = ""
}

variable "APKVERS" {
  default = ""
}

variable "CI_PIPELINE_CREATED_AT" {
  default = "now"
}

variable "CI_COMMIT_TAG" {
  default = "testing"
}

variable "CI_COMMIT_SHA" {
  default = ""
}

variable "CI_PROJECT_URL" {
  default = ""
}

variable "CI_PROJECT_PATH" {
  default = ""
}

variable "CI_REGISTRY" {
  default = ""
}

variable "DOCKER_REPO" {
  default = ""
}

variable "DOCKER_UN" {
  default = "UNSET"
}

variable "DOCKER_PW" {
  default = ""
}

variable "DOCKER_IMAGE" {
  default = "test"
}

variable "DESC" {
  default = "docker image"
}

variable "BUILD_PLAT" {
  default = "amd64"
}

target "image" {
  labels = {
    "org.label-schema.schema-version" = "1.0",
    "org.label-schema.build-date" = "${CI_PIPELINE_CREATED_AT}",
    "org.label-schema.version" = "${CI_COMMIT_TAG}",
    "org.label-schema.vcs-ref" = "${CI_COMMIT_SHA}",
    "org.label-schema.vcs-url" = "${CI_PROJECT_URL}",
    "org.label-schema.name" = "${DOCKER_REPO}",
    "org.label-schema.description" = "${DESC}",
    "alpine-version" = "${ALP_VER}",
    "apk" = "${APK}",
    "apk-version" = "${APKVERS}"
  }

  args = {
    "DVER" = "${ALPINE_RELEASE}"
  }
  tags = [ "${CI_COMMIT_TAG}" ]
}

target "test-image" {
  inherits = [ "image" ]
  platforms = [
    "linux/amd64",
    "linux/arm64"
  ]
  tags = [ "${DOCKER_IMAGE}" ]
}

target "dev-image" {
  inherits = [ "test-image" ]
  dockerfile = "Dockerfile.dev"
  tags = [ "${DOCKER_IMAGE}-dev" ]
  }

target "chimera-image" {
  inherits = [ "test-image" ]
  dockerfile = "Dockerfile.chimera"
  labels = {
    "alpine-version" = ""
  }
  tags = [ "${DOCKER_IMAGE}-chimera" ]
  }

target "multi-arch" {
  inherits = [ "image" ]
  platforms = [
    "linux/amd64",
    "linux/386",
    "linux/ppc64le",
    "linux/s390x",
    "linux/arm64",
    "linux/arm/v7"
  ]
}

target "single-arch" {
  inherits = [ "image" ]
  platforms = [ "linux/${BUILD_PLAT}" ]
}

target "docker-hub" {
  inherits = [ "multi-arch" ]
  tags = [
    "docker.io/${DOCKER_UN}/${DOCKER_REPO}",
    "docker.io/${DOCKER_UN}/${DOCKER_REPO}:${CI_COMMIT_TAG}"
  ]
}

target "gitlab" {
  inherits = [ "multi-arch" ]
  tags = [
    "${CI_REGISTRY}/${CI_PROJECT_PATH}",
    "${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_TAG}"
  ]
}
